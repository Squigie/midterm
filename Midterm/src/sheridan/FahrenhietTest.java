package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenhietTest {

	@Test
	public void testFromCelciusRegualr() {
		int celcius = Fahrenhiet.fromCelcius(0);
		assertTrue("Temperature not found.", celcius == 32);
	}
	
	@Test
	public void testFromCelciusExceptional() {
		int celcius = Fahrenhiet.fromCelcius(-10);
		assertFalse("Temperature not found.", celcius == 0);
	}
	
	@Test
	public void testFromCelciusBoundaryIn() {
		int celcius = Fahrenhiet.fromCelcius(25);
		assertTrue("Temperature not found.", celcius == 77);
	}
	
	@Test
	public void testFromCelciusBoundaryOut() {
		int celcius = Fahrenhiet.fromCelcius(-32);
		assertFalse("Temperature not found.", celcius == 0);
	}

}
